# Cookie H4pp3ns - Mod for Cookie Clicker (Steam)
## Description
Simple mod which marks the unlocked building with the lowest payback period and shows some useful info in the tooltips.\
__*Steam Achievements*__ will still work, since the mod is purely visual.

## Features
* Lightgreen tint: Buying this building has lowest payback period
* Lightgreen tooltip: Time until building(s) will generate profit
* Orange tooltip: How many instances of this building can be purchased with your current amount of cookies
* Red tooltip: How long do you have to wait until you're able to afford your current buying configuration (1,10,100)
* Show time until next prestige level
* The "Conjure Baked Goods" spell will show your payout and if it is capped by the 15% rule
* An underscore below a building's title will indicate when you can reach the next step of 50
* Cyan tooltip: Shows up when upgrading a building is cheaper than buying (except Cursor and Grandma head upgrades)
* Green tooltip: Shows up when buying this upgrade is cheaper than doubling the amount of buildings (upgrade tooltip only, except Cursor and Grandma head upgrades)
* Crimson red tooltip: Shows up when a cheaper upgrade of this kind is available (upgrade tooltip only, except Cursor and Grandma head upgrades)
* Lightgreen upgrade background: Buying this upgrade has lowest payback period (except Cursor and Grandma head upgrades)
* Cookie upgrades are considered too!
* Considers Kitten upgrades!
* Deep pink tooltip: Shows CpS gain percentage on purchase
* Stock Market shows potential profit on a per good basis, helping you find the best moment to sell (mod has to be active during purchase)
* Shows which prestige level you will reach when ascending

![Cookie H4pp3ns](cookie_h4pp3ns.png)

## Stock Market
Trades are tracked by saving the average cost and amount per good as mod save data. Saving the amount might seem redundant but allows to be kind of self healing in case trades were made when the mod wasn't loaded.

Example:
* You got no brokers = __20%__ overhead
* You buy in at __$5__ - have to pay __$6__ = $5 * 1.2
    * Profit: __-16.7%__ = ($5 / $6) - 1
* Stock rises from __$5__ to __$7.5__
    * Profit: __25%__ = ($7.5 / $6) - 1

![Cookie H4pp3ns](cookie_h4pp3ns_2.png)

Yeah the arrow is misleading and pointless, but i like it...

## Installation
* Go to __*C:\Program Files (x86)\Steam\steamapps\common\Cookie Clicker\resources\app\mods\local*__
* Place __*info.txt*__ and __*main.js*__ in a new folder called __*CookieH4pp3ns*__
* Restart the game
* Go to __*Options*__, scroll down and click __*Manage Mods*__
* Choose __*Cookie H4pp3ns*__ from the list
* Press __*Enable*__
* Press __*Restart with new changes*__

## What does it do?
The mod calculates the time a newly purchased building takes to pay for itself. Meaning the timeframe until you profit from your purchase.\
So in essence `Cookies / CpS = seconds_until_equilibrium`.\
Also works with different batch sizes (1,10,100) and won't show anything in selling mode.

Example: Grandma currently costs 115 and will provide +1 CpS.\
`115 Cookies / (1 Cookies/s) = 115 s`\
Meaning Grandma will generate profit after 1 minute and 55 seconds.

## How does it work?
The Steam port is basically the browser version running in Electron, meaning it is still just JavaScript.\
If you want to explore by yourself just open the [browser version](https://orteil.dashnet.org/cookieclicker/) hit F12 (or whatever your debug-console hotkey is) and have fun.\
Most of the functionality is accessible through the `Game` object.

If you want to work on the Steam version directly, you can also go to __*C:\Program Files (x86)\Steam\steamapps\common\Cookie Clicker\resources\app\start.js*__ and change __DEV=0__ to __DEV=1__. The game will start with Debug tools enabled.\
__Steam Achievements__ wont trigger while __DEV=1__ is active. Just switch it back to zero and restart the game. Your Achievements will trigger after a short while.

For your first mod of the steam version the sample mods might be helpful.\
Also take a look at the games source code in the __*C:\Program Files (x86)\Steam\steamapps\common\Cookie Clicker\resources\app\src\main.js*__ and search for __*MODDING API*__ or any part of the game you are interested in.

## What's more to come?
Idk man... possibly anything I personally don't see as cheating.
* Signal if buying an upgrade is more profitable than buying a building
* Helpers for some of the minigames
* Consider duration of temporary boosts when doing calculations
* Keep track of buying prices in Stock Market

## Why the name?
I mean... the alternative would have been Sh1t Clicker, so I hope you understand.

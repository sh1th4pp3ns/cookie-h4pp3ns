Game.registerMod("cookie h4pp3ns",{
    version: 0.63,
    saving: true,
    crateTooltip: null, // is set during upgrade injection
    buildings: [],
    minigames: {},
    cookieUpgradePbp: [],
    kittenTierMultipliers: {1: 0.1, 2: 0.125, 3: 0.15, 4: 0.175, 5: 0.2, 6: 0.2, 7: 0.2, 8: 0.2, 9: 0.2, 10: 0.175, 11: 0.15, 12: 0.125, 13: 0.115},
    kittenPbp: [],
    init:function(){
        // initialize mod on startup
        let MOD = this;

        // inject tooltips of buildings
        // one by one just to be safe
        Game.ObjectsById.forEach(building => {
            MOD.buildings[building.name] = {}
            MOD.injectBuildingTooltip(building);
        });

        // inject tooltip of upgrade store
        MOD.injectUpgradeTooltip();

        // inject minigames
        MOD.injectGrimoire();
        MOD.injectMarket();

        // do this on every logic tick
        Game.registerHook("logic", function(){
            MOD.markBestPbp();
            MOD.injectAscendTooltip();
        });

        Game.Notify("Cookie H4pp3ns loaded!","",[16,5]);
    },
    save:function(){
        let MOD = this;

        if(MOD.saving) {
            let saveData = {};

            if(typeof MOD.minigames["Stock Market"] !== 'undefined' && Object.keys(MOD.minigames["Stock Market"].goods).length > 0) {
                saveData.goods = MOD.minigames["Stock Market"].goods;
            }

            if(Object.keys(saveData).length > 0) {
                saveData.modVersion = MOD.version;
                return JSON.stringify(saveData);
            }

            return "";
        }
        else {
            Game.Notify("Cookie H4pp3ns: Saving mod data is still disabled due to an error while loading","",[16,5]);
        }
    },
    load:function(str){
        let MOD = this;

        if(str !== "") {
            try {
                let saveData=JSON.parse(str);

                if(saveData.modVersion > MOD.version) {
                    throw new Error("mod version: " + MOD.version + ", mod data version: " + saveData.modVersion + " - PLEASE UPDATE");
                }

                if (saveData.goods) {
                    MOD.minigames["Stock Market"].goods = saveData.goods;
                } 
            }
            catch (e){
                Game.Notify("Cookie H4pp3ns: Error loading save data: " + e.message,"",[16,5]);
                console.error(e);

                MOD.saving = false;
                Game.Notify("Cookie H4pp3ns: Saving mod data has been disabled to protect your save","",[16,5]);
            }
        }
    },
    injectBuildingTooltip: function(building) {
        let MOD = this;

        MOD.buildings[building.name].tooltip = building.tooltip;
        building.tooltip = function tooltip() {
            return Game.mods["cookie h4pp3ns"].editBuildingTooltip(this);
        };
    },
    editBuildingTooltip: function(building) {
        let MOD = this;

        // get original tooltip 
        let tt = MOD.buildings[building.name].tooltip.apply(building);

        // inject into tooltip when in buy mode and building is unlocked
        if(Game.buyMode == 1 && building.locked == 0) {
            if(MOD.buildings[building.name].cheapestUpgradePbp <= MOD.buildings[building.name].pbp) {
                let toBeInjected = 'Upgrade cheaper than buying';
                if(Game.buyBulk > 1) {
                    toBeInjected += " " + Game.buyBulk;
                }

                tt = MOD.addToBuildingTooltip(tt, toBeInjected, "background-color:cyan; color:black");
            }

            let toBeInjected = MOD.sayTime(MOD.buildings[building.name].pbp) + ' to pay for itself';
            tt = MOD.addToBuildingTooltip(tt, toBeInjected, "background-color:lightgreen; color:black");

            toBeInjected = "+" + Math.round(building.storedCps * Game.buyBulk / Game.buildingCps * 1000)/10 + "% CpS";
            tt = MOD.addToBuildingTooltip(tt, toBeInjected, "background-color:deeppink; color:white");

            if(MOD.buildings[building.name].buyingPower > 0) {
                toBeInjected = MOD.buildings[building.name].buyingPower + ' can be bought';
                tt = MOD.addToBuildingTooltip(tt, toBeInjected, "background-color:#ffa500; color:black;");
            }
        }

        if(MOD.buildings[building.name].timeUntilAffordable > 0) {
            let toBeInjected = MOD.sayTime(MOD.buildings[building.name].timeUntilAffordable) + ' until affordable';
            tt = MOD.addToBuildingTooltip(tt, toBeInjected, "background-color:darkred; color:white");
        }

        return tt;
    },
    addToBuildingTooltip:function(tooltip, toBeInjected, style = ""){
        if(typeof Steam !== 'undefined') {
            return tooltip.replace('<div class="tag">', '<div class="tag" style="' + style + '">' + toBeInjected + '</div><div class="tag">');
        }
        else {
            return tooltip.replace('<small>', '<small style="text-shadow: none;' + style + '">[' + toBeInjected + ']</small> <small>');
        }
    },
    injectUpgradeTooltip: function() {
        let MOD = this;

        MOD.crateTooltip = Game.crateTooltip;
        Game.crateTooltip =  function crateTooltip(me, context) {
            let tt = MOD.crateTooltip(me, context);

            if(context == 'store') {
                if(me.tier >= 0 && typeof me.buildingTie.name !== 'undefined' && me.buildingTie.name in MOD.buildings) {
                    if(MOD.buildings[me.buildingTie.name].cheaperUpgradeId == me.id) {
                        if(MOD.buildings[me.buildingTie.name].cheapestUpgradePbp <= MOD.buildings[me.buildingTie.name].pbp) {
                            let toBeInjected = "Upgrade cheaper than buying";
                            if(Game.buyBulk > 1) {
                                toBeInjected += " " + Game.buyBulk;
                            }

                            tt = MOD.addToUpgradeTooltip(tt, toBeInjected, "background-color:cyan; color:black");
                        }
                        else {
                            //let toBeInjected = "Upgrade cheaper than doubling";
                            //tt = MOD.addToUpgradeTooltip(tt, toBeInjected, "background-color:green; color:white");
                        }

                        let toBeInjected = MOD.sayTime(MOD.buildings[me.buildingTie.name].cheapestUpgradePbp) + " to pay for itself";
                        tt = MOD.addToUpgradeTooltip(tt, toBeInjected, "background-color:lightgreen; color:black");

                        toBeInjected = "+" + Math.round(Game.Objects[me.buildingTie.name].storedTotalCps / Game.buildingCps * 1000)/10 + "% CpS";
                        tt = MOD.addToUpgradeTooltip(tt, toBeInjected, "background-color:deeppink; color:white");
                    }
                    else if(me.buildingTie.tieredUpgrades.includes(me)) {
                        let toBeInjected = "Not worth it";
                        tt = MOD.addToUpgradeTooltip(tt, toBeInjected, "background-color:crimson; color:white");
                    }
                }

                if(Game.UpgradesByPool["cookie"].includes(me)) {
                    let index = Game.UpgradesInStore.indexOf(me);

                    if(MOD.cookieUpgradePbp[index] == MOD.cookieUpgradePbp.slice().sort((a,b) => a - b)[0]) {
                        let toBeInjected = MOD.sayTime(MOD.cookieUpgradePbp[index]) + " to pay for itself";
                        tt = MOD.addToUpgradeTooltip(tt, toBeInjected, "background-color:lightgreen; color:black");
                    }
                    else {
                        let toBeInjected = "Not worth it";
                        tt = MOD.addToUpgradeTooltip(tt, toBeInjected, "background-color:crimson; color:white");
                    }
                    
                }
                else if(Game.UpgradesByPool["kitten"].includes(me) && me.tier > 0) {
                    let index = Game.UpgradesInStore.indexOf(me);

                    if(MOD.kittenPbp[index] == MOD.kittenPbp.slice().sort((a,b) => a - b)[0]) {
                        let toBeInjected = MOD.sayTime(MOD.kittenPbp[index]) + " to pay for itself";
                        tt = MOD.addToUpgradeTooltip(tt, toBeInjected, "background-color:lightgreen; color:black");
                    }
                    else {
                        let toBeInjected = "Not worth it";
                        tt = MOD.addToUpgradeTooltip(tt, toBeInjected, "background-color:crimson; color:white");
                    }
                    
                    let toBeInjected = "+" + Math.round(MOD.kittenTierMultipliers[me.tier] * Game.milkProgress * 1000)/10 + "% CpS";
                    tt = MOD.addToUpgradeTooltip(tt, toBeInjected, "background-color:deeppink; color:white");
                }

                let neededTime = MOD.affordableIn(me.getPrice());
                if(neededTime > 0) {
                    let toBeInjected = MOD.sayTime(neededTime) + ' until affordable';
                    tt = MOD.addToUpgradeTooltip(tt, toBeInjected, "background-color:darkred; color:white");
                }

            }

            return tt;
        }
    },
    addToUpgradeTooltip:function(tooltip, toBeInjected, style = ""){
        if(typeof Steam !== 'undefined') {
            return tooltip.replace('<div class="tag"', '<div  class="tag" style="' + style + '">' + toBeInjected + '</div><div class="tag"');
        }
        else {
            return tooltip.replace('<div class="tag"', '<div  class="tag" style="text-shadow: none;' + style + '">[' + toBeInjected + ']</div> <div class="tag"');
        }
    },
    injectAscendTooltip:function(){
        let MOD = this;

        if(Game.T%15==0) {
            let chipsOwned=Game.HowMuchPrestige(Game.cookiesReset);
            let ascendNowToOwn=Math.floor(Game.HowMuchPrestige(Game.cookiesReset+Game.cookiesEarned));
            let ascendNowToGet=ascendNowToOwn-Math.floor(chipsOwned);
            let cookiesToNext=Game.HowManyCookiesReset(ascendNowToOwn+1)-(Game.cookiesEarned+Game.cookiesReset);
            let neededTime = MOD.gainedIn(cookiesToNext);

            let element = l("ascendTooltip");
            element.innerHTML += '<div class="line"></div><b>' + MOD.sayTime(neededTime) + '</b> until next level<br>Prestige level after ascending: <b>' + SimpleBeautify(Game.prestige + ascendNowToGet) + '</b>';
        }
    },
    gainedIn:function(cookies){
        return cookies / (Game.cookiesPs*(1-Game.cpsSucked));
    },
    getMissingCookies:function(price){
        return Math.max(price - Game.cookies, 0);
    },
    affordableIn:function(price){
        return this.gainedIn(this.getMissingCookies(price));
    },
    calcBuyingPower:function(building){ // calculate how many buildings of this type could be purchased
        let priceMultiplier = 1 / Game.modifyBuildingPrice(building, 1);
        let cookies = Game.cookies * priceMultiplier;

        let purchased = building.amount - building.free;
        return Math.floor((Math.log((0.15 * cookies / building.basePrice) + Game.priceIncrease**purchased) / Math.log(Game.priceIncrease)) - purchased);
    },
    markBestPbp:function(){ // calculate pbp (pay back period in seconds) and mark building with the lowest
        let MOD = this;
        let lowestPbpBuilding = "";
        let lowestPbp = Number.MAX_VALUE;

        Game.ObjectsById.forEach(building => {
            // reset color on every buy button
            building.l.querySelector("div.content div.title").style.color = "";

            let availableUpgrades = building.tieredUpgrades.filter(upgrade => upgrade.tier > 0 && upgrade.bought == 0 && upgrade.unlocked == 1);
            MOD.buildings[building.name].cheaperUpgradeId = availableUpgrades.map(upgrade => upgrade.id).concat(-1)[0];
            MOD.buildings[building.name].cheapestUpgradePbp = availableUpgrades.map(upgrade => MOD.affordableIn(upgrade.getPrice()) + (upgrade.getPrice() / (building.storedTotalCps * Game.globalCpsMult))).concat(Number.MAX_VALUE)[0];

            MOD.buildings[building.name].timeUntilAffordable = MOD.affordableIn(building.bulkPrice);
            if(Game.buyMode == 1 && building.locked == 0) {
                // calculate pbp and save it for tooltip
                MOD.buildings[building.name].pbp = MOD.buildings[building.name].timeUntilAffordable + (building.bulkPrice / (building.storedCps * Game.globalCpsMult * Game.buyBulk));
                
                // if pbp ist better (lower) than the previous, save it
                let minPbp = Math.min(MOD.buildings[building.name].pbp, MOD.buildings[building.name].cheapestUpgradePbp);
                if(minPbp <= lowestPbp) {
                    lowestPbpBuilding = building;
                    lowestPbp = minPbp;
                }
            }

            MOD.buildings[building.name].buyingPower = MOD.calcBuyingPower(building);
            let firstLevels = Math.max(0, building.amount - building.highest + MOD.buildings[building.name].buyingPower);
            if((building.highest % 50 + firstLevels) >= 50) {
                building.l.querySelector("div.content div.title").style.textDecoration = 'underline';
            }
            else {
                building.l.querySelector("div.content div.title").style.textDecoration = '';
            }
        });

        MOD.kittenPbp = [];
        MOD.cookieUpgradePbp = [];
        Game.UpgradesInStore.forEach((upgrade, index) => {
            MOD.upgradeDiv(upgrade).style.backgroundColor = '';

            if(Game.UpgradesByPool["cookie"].includes(upgrade)) {
                MOD.cookieUpgradePbp[index] = MOD.affordableIn(upgrade.getPrice()) + (upgrade.getPrice() / (Game.buildingCps * Game.globalCpsMult * upgrade.power / 100));
            }
            else if(Game.UpgradesByPool["kitten"].includes(upgrade) && upgrade.tier > 0) {
                MOD.kittenPbp[index] = MOD.affordableIn(upgrade.getPrice()) + (upgrade.getPrice() / (Game.buildingCps * Game.globalCpsMult * MOD.kittenTierMultipliers[upgrade.tier] * Game.milkProgress));
            }
        });

        let lowestCookieUpgradePbP = MOD.cookieUpgradePbp.concat(Number.MAX_VALUE).sort((a,b) => a - b)[0];
        let lowestKittenPbp = MOD.kittenPbp.concat(Number.MAX_VALUE).sort((a,b) => a - b)[0];
        let overallLowestPbp = Math.min(lowestPbp, Math.min(lowestCookieUpgradePbP, lowestKittenPbp));
        if(lowestKittenPbp <= overallLowestPbp && overallLowestPbp < Number.MAX_VALUE) {
            let index = MOD.kittenPbp.indexOf(lowestKittenPbp);
            MOD.upgradeDiv(Game.UpgradesInStore[index]).style.backgroundColor = 'lightgreen';
        }
        else if(lowestCookieUpgradePbP <= overallLowestPbp && overallLowestPbp < Number.MAX_VALUE) {
            let index = MOD.cookieUpgradePbp.indexOf(lowestCookieUpgradePbP);
            MOD.upgradeDiv(Game.UpgradesInStore[index]).style.backgroundColor = 'lightgreen';
        }
        else if(lowestPbpBuilding != "") {
            if(MOD.buildings[lowestPbpBuilding.name].cheapestUpgradePbp <= lowestPbp) {
                let div = MOD.upgradeDiv(Game.UpgradesById[MOD.buildings[lowestPbpBuilding.name].cheaperUpgradeId]);
                if(div !== null) {
                    div.style.backgroundColor = 'lightgreen';
                }
            }
            else {
                // mark the best building
                lowestPbpBuilding.l.querySelector("div.content div.title").style.color = "lightgreen";
            }
        }
    },
    sayTime:function(seconds){
        return Game.sayTime(seconds*Game.fps, -1);
    },
    injectGrimoire:function(){
        let MOD = this;

        Game.registerHook("logic", function grimInjector(){
            let tower = Game.Objects["Wizard tower"];

            // check if minigame is active
            if(typeof tower.minigame !== 'undefined') {
                let grim = tower.minigame;
                MOD.minigames[grim.name] = {};
                MOD.minigames[grim.name].spellTooltip = grim.spellTooltip;

                let spellId = grim.spells["conjure baked goods"].id;

                grim.spellTooltip = function(id) {
                    if(id == spellId) {
                        return function() {
                            let tooltip = Game.mods["cookie h4pp3ns"].minigames["Grimoire"].spellTooltip(id)();
                            
                            let possible = Game.cookiesPs*60*30;
                            let maximum = Game.cookies*0.15;

                            if(maximum < possible) {
                                return tooltip.replace('<div class="line">', '<div class="tag">max payout ' + Beautify(possible) + '</div> <div class="tag" style="background-color:red">actual payout ' + Beautify(maximum) + '</div><div class="line">');
                            }
                            else {
                                return tooltip.replace('<div class="line">', '<div class="tag" style="background-color:green">payout ' + Beautify(possible) + '</div><div class="line">');
                            }       
                        }
                    }
                    return Game.mods["cookie h4pp3ns"].minigames["Grimoire"].spellTooltip(id);
                }

                // injection successful, stop trying
                Game.removeHook("logic", grimInjector);
            }
        });
    },
    upgradeDiv:function(upgrade){
        return l("upgrade" + Game.UpgradesInStore.indexOf(upgrade));
    },
    injectMarket:function(){
        let MOD = this;
        MOD.minigames["Stock Market"] = {goods: {}};

        Game.registerHook("logic", function marketInjector(){
            // check if minigame is active
            let bank = Game.Objects["Bank"];

            if(typeof bank.minigame !== 'undefined') {
                let market = bank.minigame;

                MOD.minigames[market.name].buyGood = market.buyGood;
                market.buyGood = function buyGood(id, n) {
                    let countBefore = market.goodsById[id].stock;
                    let profitBefore = market.profit;
                    let val = Game.mods["cookie h4pp3ns"].minigames["Stock Market"].buyGood.apply(market, [id, n]);
                    let count = market.goodsById[id].stock - countBefore;
                    let price = (profitBefore - market.profit) / count;

                    if(val) {
                        Game.mods["cookie h4pp3ns"].marketGoodBought(market, id, count, price);
                    }

                    return val;
                };

                MOD.minigames[market.name].sellGood = market.sellGood;
                market.sellGood = function sellGood(id, n) {
                    let val = Game.mods["cookie h4pp3ns"].minigames["Stock Market"].sellGood.apply(market, [id, n]);
                    
                    if(val) {
                        Game.mods["cookie h4pp3ns"].marketGoodSold(market, id);
                    }

                    return val;
                };

                MOD.minigames[market.name].tick = market.tick;
                market.tick = function tick() {
                    let val = Game.mods["cookie h4pp3ns"].minigames["Stock Market"].tick.apply(market);
                    
                    Game.mods["cookie h4pp3ns"].marketTick(market);

                    return val;
                };

                MOD.initMarketData(market);

                // injection successful, stop trying
                Game.removeHook("logic", marketInjector);
            }
        });
    },
    initMarketData:function(market){
        let MOD = this;

        market.goodsById.forEach(good => {
            let container = document.createElement("div");
            container.id = "bankGood-" + good.id + "-cookieH4pp3ns-stockBox";

            let profitDiv = good.stockBoxL.cloneNode(false);
            profitDiv.className = "bankSymbol";
            profitDiv.textContent = "profit: ";

            let profitSpan = document.createElement("span");
            profitSpan.textContent = "-";
            profitSpan.className = "";
            profitDiv.appendChild(profitSpan);

            container.appendChild(profitDiv);


            let avgDiv = good.stockBoxL.cloneNode(false);
            avgDiv.className = "bankSymbol";
            avgDiv.textContent = "avg. price: ";

            let avgSpan = document.createElement("span");
            avgSpan.textContent = "-";
            avgSpan.className = "";
            avgSpan.style.fontWeight = "bold";
            avgDiv.appendChild(avgSpan);

            container.appendChild(avgDiv);


            good.stockBoxL.insertAdjacentElement('beforebegin', container);

            MOD.marketGoodSold(market, good.id);
        });
    },
    marketTick:function(market){
        let MOD = this;
        let goodsData = MOD.minigames[market.name].goods;

        market.goodsById.forEach(good => {
            MOD.marketUpdateGood(market, good);
        });
    },
    marketUpdateGood:function(market, good){
        let MOD = this;
        let goodsData = MOD.minigames[market.name].goods;

        let container = l("bankGood-" + good.id + "-cookieH4pp3ns-stockBox");

        let profitDiv = container.firstElementChild;
        let profitSpan = profitDiv.firstElementChild;

        let avgDiv = profitDiv.nextSibling;
        let avgSpan = avgDiv.firstElementChild;

        if(goodsData.hasOwnProperty(good.id)) {
            // calc profit
            let data = goodsData[good.id];

            avgSpan.textContent = "$" + data.cost;

            let profit = (good.val / data.cost) - 1;
            profit = Math.round(profit * 1000) / 10;

            profitSpan.textContent = profit + "%";
            if(profit > 0) {
                profitSpan.className = "bankSymbolNum bankSymbolUp";
            }
            else if(profit < 0) {
                profitSpan.className = "bankSymbolNum bankSymbolDown";
            }
            else {
                profitSpan.className = "bankSymbolNum";
            }
        }
        else {
            profitSpan.className = "";
            profitSpan.textContent = "-";
            avgSpan.textContent = "-";
        }
    },
    marketGoodBought:function(market, id, n, price){
        let MOD = this;
        let good = market.goodsById[id];
        let goodsData = MOD.minigames[market.name].goods;

        if(typeof goodsData[id] === 'undefined' || goodsData[id].n + n != good.stock) {
            goodsData[id] = {cost: price};
        }
        else {
            goodsData[id].cost = ((goodsData[id].n * goodsData[id].cost) + (n * price)) / (goodsData[id].n + n);
        }
        goodsData[id].n = good.stock;
        goodsData[id].cost = Math.round(goodsData[id].cost * 100) / 100;

        MOD.marketUpdateGood(market, good);
    },
    marketGoodSold:function(market, id){
        let MOD = this;
        let good = market.goodsById[id];
        let goodsData = MOD.minigames[market.name].goods;

        if(typeof goodsData[id] === 'undefined') {
            goodsData[id] = {cost: good.val};
        }

        goodsData[id].n = good.stock;

        if(good.stock == 0) {
            delete goodsData[id];
        }

        MOD.marketUpdateGood(market, good);
    },
});